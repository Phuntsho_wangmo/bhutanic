package controller

import (
	"database/sql"
	"encoding/json"
	"goproject/model"
	"goproject/utils/httpResp"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func Regitration(w http.ResponseWriter, r *http.Request){
	var user model.User
	decoder:=json.NewDecoder(r.Body)
	if err:= decoder.Decode(&user); err!=nil{
		httpResp.RespondWithError(w, http.StatusBadRequest,"invalid json body")
		return
	};
	defer r.Body.Close()
	saveErr:= user.Create()
	if saveErr!= nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return

	}
	httpResp.ResponseWithJson(w, http.StatusCreated, map[string]string{"status": "admin added"})
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	// get url parameter
	email := mux.Vars(r)["email"]
	Uemail, idErr := getuseremail(email)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	u:= model.User{Email: Uemail}

	getErr := u.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "user not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, u)
}

// helper function to receive string sid as a input and return a string s
func getuseremail(useremailParam string) (string, error) {
	return useremailParam, nil
}

// update
func UpdateUser(w http.ResponseWriter, r *http.Request){
	old_email := mux.Vars(r)["email"]
	old_Email, idErr:=getuseremail(old_email)
	if idErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	var reg model.User
	decoder:= json.NewDecoder(r.Body)
	if err:= decoder.Decode(&reg); err!=nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	updateErr := reg.Update(old_Email)
	if updateErr!= nil{
		switch updateErr{
			case sql.ErrNoRows:
				httpResp.RespondWithError(w, http.StatusNotFound, "user not found")
			default:
				httpResp.RespondWithError(w,http.StatusInternalServerError, updateErr.Error())
		}
		}else{
			httpResp.ResponseWithJson(w, http.StatusOK,reg)
	}
	}

	func DeleteUser(w http.ResponseWriter, r *http.Request){
		email := mux.Vars(r)["email"]
		Emaill, idErr := getuseremail(email)
		if idErr != nil{
			httpResp.RespondWithError(w,http.StatusBadRequest, idErr.Error())
			return
		}
		u := model.User{Email:Emaill}
		if err := u.Delete(); err !=nil{
			httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
			return
		}
		httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"status":"deleted"})
		}

	// geet all
	func GetAllUser(w http.ResponseWriter, r *http.Request){
		users, getErr := model.GetAllUser()
		if getErr != nil{
			httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
			return
		}
		httpResp.ResponseWithJson(w, http.StatusOK,users)
		}
		// login

func Login(w http.ResponseWriter, r *http.Request){
	var use model.User
	err := json.NewDecoder(r.Body).Decode(&use)
	if err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	getErr := use.Get()
	if getErr != nil{
		httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())

		return
	}
	// httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"message":"Success"})

		// // create cookie
		cookie:=http.Cookie{
			Name:"my-cookie",
			Value: "cookie-value",
			Expires: time.Now().Add(20 * time.Minute),
			Secure:true,
		}
		//set cookie and send back to client
		http.SetCookie(w, &cookie)
		httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"message":
		"success"})
	
}

// verify cookie
func verifyCookie(w http.ResponseWriter, r *http.Request)bool{
	cookie, err:=r.Cookie("my-cookie")
	if err !=nil{
		if err== http.ErrNoCookie{
			httpResp.RespondWithError(w, http.StatusSeeOther,"cookie not set")
			return false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError,
			"internal server error")
		return false
	}
	//validate cookie value
	if cookie.Value!="cookie-value"{
		httpResp.RespondWithError(w, http.StatusSeeOther, "cookie value does not match")
		return false
	}
	return true
}