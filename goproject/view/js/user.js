window.onload = function(){
    fetch("/registers")
        .then(response => response.text())
        .then(data => GetAllUser(data))

        // Backtrack();
}



async function GetAllUser(data){

    const users = JSON.parse(data)
    users.forEach(user =>{
    

        var table = document.getElementById("table")

        var row = table.insertRow();

        var td=[]

        for(i=0; i<table.rows[0].cells.length;i++){
            td[i] = row.insertCell(i)
        }
        td[0].innerHTML = user.Username;
        td[1].innerHTML = user.Email;
        td[2].innerHTML = user.Phone_Number;
        td[3].innerHTML = '<input type = "button" onclick="DeleteUser(this)" value="Delete" id="buttonDelete">'
       

    })

}

var selectedRow = null;



  

const DeleteUser = async (r) =>{
    if(confirm("Are You sure you want to DELETE this?")){
        selectedRow = r.parentElement.parentElement;
        sid = selectedRow.cells[1].innerHTML

        fetch('/register/'+sid, {
            method: "DELETE",
            headers: {"Content-type": "application/json; charset=UTF-8"}
        });
        var rowIndex = selectedRow.rowIndex;
        if (rowIndex>0) {
            document.getElementById("table").deleteRow(rowIndex);

        }
        selectedRow = null;
    }
}
