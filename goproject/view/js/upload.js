window.onload = function(){
    fetch("/products")
        .then(response => response.text())
        .then(data => GetAllProduct(data))

}




async function GetAllProduct(data){
    const products = JSON.parse(data)
    products.forEach(product =>{
        var container = document.getElementById("container")
        console.log(product)

        var div1 = document.createElement('div')
        div1.className = "grid-item";
        container.appendChild(div1)

        var img = document.createElement('img')
        img.className = "nav_img"
        img.setAttribute('src', product.Picture)
        img.setAttribute('alt', " Picture")
        div1.appendChild(img)
    //    console.log(product.Picture)
        var p1 = document.createElement('p')
        p1.style = "padding-left:10px; padding-top:0px; padding-bottom:2px;"
        p1.className = "detail_of_art"
        p1.innerHTML = product.Name
        div1.appendChild(p1)

        var p2 = document.createElement('p')
        p2.className = "detail_of_art"
        p2.style = "padding-left:10px; padding-top:0px; padding-bottom:2px;"
        p2.innerHTML= "Nu."+product.Price
        div1.appendChild(p2)

        var p3 = document.createElement('p')
        p3.className = "button_container"
        div1.appendChild(p3)

        var button = document.createElement('button')
        button.className="buy_button"
        button.style = "padding-left:10px; padding-top:0px; margin-top:0rem; padding-bottom:0px;"
        button.innerHTML="Click To Buy"
        p3.appendChild(button)
        
        button.addEventListener("click", () => {
            const session = checkSession()

            if (session === true) {
                button.setAttribute("onclick", "Buy()")
            } else {
                alert("Please Login")
                location.assign("login.html")
            }
        });
    })
}

function checkSession() {
    const sessionExists = !!localStorage.getItem('email');

    if (!sessionExists) {
        return false
    }
    return true;
}



function Buy(){
    alert("item bought")
}