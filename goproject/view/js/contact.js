function addContact() {
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var message = document.getElementById("message").value;

  // Check if any field is empty
  if (name.trim() === "" || email.trim() === "" || message.trim() === "") {
    alert("Please fill in all the fields.");
    return;
  }

  var _data = {
    Name: name,
    Email: email,
    Message: message,
  };

  fetch("/contact", {
    method: "POST",
    body: JSON.stringify(_data),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => {
      if (response.ok) {
        resetForm();
        alert("Contact added successfully!");
      } else {
        throw new Error("Failed to add contact");
      }
    })
    .catch((error) => {
      console.log("Error:", error);
      alert("Failed to add contact");
    });
}

function resetForm() {
  document.getElementById("name").value = "";
  document.getElementById("email").value = "";
  document.getElementById("message").value = "";
}
