package routes

import (
	"goproject/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitializedRoutes() {
    router := mux.NewRouter()

    // registration
    router.HandleFunc("/register", controller.Regitration).Methods("POST")
    router.HandleFunc("/register/{email}",controller.GetUser).Methods("GET")
    router.HandleFunc("/register/{email}", controller.UpdateUser).Methods("PUT")
    router.HandleFunc("/register/{email}", controller.DeleteUser).Methods("DELETE")
    router.HandleFunc("/registers", controller.GetAllUser)

    // login
    router.HandleFunc("/login",controller.Login).Methods("POST")
   
//    product
    router.HandleFunc("/product", controller.AddProduct).Methods("POST")
    router.HandleFunc("/product/{pid}", controller.GetProduct).Methods("GET")
    router.HandleFunc("/product/{pid}", controller.DeleteProduct).Methods("DELETE")
    router.HandleFunc("/product/{pid}", controller.UpdateProduct).Methods("PUT")
    router.HandleFunc("/products", controller.GetAllProduct).Methods("GET")

   

    

    // to serve static files
    fhandler := http.FileServer(http.Dir("./view"))
    router.PathPrefix("/").Handler(fhandler)

    // start the server
    err := http.ListenAndServe(":8081", router)
    if err != nil {
        os.Exit(1)
    }
}


