package model

import postgres "goproject/datastore/Postgress"

type Profile struct {
	Pid   int 
	Picture string
}
const (
	quertInsertprofile = "Insert into profile(pid,picture) VALUES($1,$2)"
	queryGetprofile  = "Select pid,name,price,picture FROM product where pid = $1"
	// queryDeleteproduct = "Delete FROM product where pid = $1"
	// queryUpdateProduct= "UPDATE product SET pid=$1,name=$2, price=$3, picture=$4 WHERE pid=$5 RETURNING pid"
)
func (pro *Profile) Savee() error {
	stmt, err := postgres.Db.Prepare(quertInsertprofile)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, saveErr := stmt.Exec(pro.Pid, pro.Picture)
	if saveErr != nil {
		return saveErr
	}
	return nil
}
// get
func (pro *Profile) GetProfile() error {
	stmt, err := postgres.Db.Prepare(queryGetprofile)
	if err != nil {
		return err
	}
	defer stmt.Close()

	result := stmt.QueryRow(pro.Pid)
	getErr := result.Scan(&pro.Pid, &pro.Picture)
	if getErr != nil {
		return getErr
	}
	return nil
}

// // delete
// func (p *Product) Delete() error {
// 	stmt, err := postgres.Db.Prepare(queryDeleteproduct)

// 	if err != nil {
// 		return err
// 	}
// 	defer stmt.Close()

// 	if _, err = stmt.Exec(p.Pid); err != nil {
// 		return err
// 	}
// 	return nil
// }



// // func (p *Product) Update(pid int) error {
// // 	stmt, updateErr := postgres.Db.Prepare(queryUpdateProduct)

// // 	if updateErr != nil {
// // 		return updateErr
// // 	}
// // 	defer stmt.Close()

// // 	_, updateErr = stmt.Exec(p.Pid, p.Name, p.Price,p.Picture, pid)

// // 	if updateErr != nil {
// // 		return updateErr
// // 	}
// // 	return nil
// // }
// func(p *Product)Update(old_pid int) error{
// 	err:=postgres.Db.QueryRow(queryUpdateProduct, p.Pid, p.Name, p.Price, p.Picture,  old_pid).Scan(&p.Pid)
// 	return err
// }